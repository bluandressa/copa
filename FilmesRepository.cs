using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Copa
{
    public class FilmesRepository
    {

        HttpClient cliente = new HttpClient();

        public FilmesRepository()
        {
            cliente.BaseAddress = new Uri("http://copafilmes.azurewebsites.net/");

            cliente.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public async Task<List<Filmes>> GetFilmesAsync()
        {
            HttpResponseMessage response = await cliente.GetAsync("api/filmes");
            if (response.IsSuccessStatusCode)
            {
                var dados = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<List<Filmes>>(dados);
            }
            return new List<Filmes>();        
        }
    }
}