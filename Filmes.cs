namespace Copa
{
    public class Filmes
    {
        public string id { get; set;}
        public string titulo {get; set;}
        public int ano{get; set;}
        public float nota {get;set;}

        public override string ToString()
        {
            return string.Format($"{id} : {titulo} :{ano} : {nota}");

        }
    }
}