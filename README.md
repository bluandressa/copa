## Copa Filmes, é um projeto desenvolvido com Node.Js v8.11.2  e Dotnet 3.1


* Primeiro Passo é clonar o Repositório


## Instalações Realizadas: 

* Se ainda não possui Node.Js ou Dotnet instalado em seu computador, segue alguns tutorias de instalação:


## DOTNET

Tutorial Microsoft: https://docs.microsoft.com/pt-br/dotnet/core/install/linux-package-manager-ubuntu-1910


* Dentro da Pasta copa, com o comando 'dotnet run', é realizado o request para a API. 


## Node.Js

Tutorial: https://www.devmedia.com.br/como-instalar-o-node-js-npm-e-o-react-no-windows/40329


* Dentro da pasta copa_filmes, com o comando 'npm start', inicializamos a página


## No navegador


 * Coloque a URL - http://localhost:3000/

