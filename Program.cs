﻿using System;
using System.Threading.Tasks;
using static System.Console;
namespace Copa
{
    class Program
    {
        static void Main(string[] args)
        {
            WriteLine("Solicitando API");
            var repository = new FilmesRepository();
            var filmesTask = repository.GetFilmesAsync();

            filmesTask.ContinueWith(task =>
            {
                var filmes = task.Result;
                foreach(var filme in filmes)
                    WriteLine(filme.ToString());

                Environment.Exit(0);               
            },
            TaskContinuationOptions.OnlyOnRanToCompletion
            );
            ReadLine();
        }

    }
}
