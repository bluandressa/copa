import React from 'react';
import {Title, TR} from "../style";

export default class App extends React.Component {
    render() {
        return (
            <div>
                <Title>Filmes</Title>
                    <table className="tabela_filmes">
                        <thead>
                            <tr>
                                <input type="checkbox"></input>
                                <th scope="col"> Filme 1</th>
                            </tr>
                            <tr>
                                <input type="checkbox"></input>
                                <th scope="col"> Filme 7</th>
                            </tr>
                            <tr>
                                <input type="checkbox"></input>
                                <th scope="col"> Filme 2</th>
                            </tr>
                        </thead>
                    </table>
            </div>
        );
    }
}
